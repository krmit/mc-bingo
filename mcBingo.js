var fs = require("fs");
var path = require("path");
var express = require("express");
var bodyParser = require("body-parser");
var colors = require("colors");
var mustache = require("mustache");
var mysql = require("mysql");
var config = require("./config.js");

var argv = require("yargs")
.usage("Usage: mcBingo.js [options] [port]")
.alias("p", "port")
.nargs("port",1)
.describe("port", "Witch port the server will lissen to.")
.alias("h", "help")
.help("h")
.epilog("copyright 2016 by Magnus Kronnäs")
.argv;

var app = express();

app.use(bodyParser.urlencoded({
    extended: true
}));


var port = 1337;

var connection = mysql.createConnection(config);
connection.connect();

if("port" in argv) {
    port=argv["port"];
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
}

// Path to dir with templates.
var dir_path = path.resolve(__dirname+"/html/");

// First page
app.get("/", function (request, respons) {
    var page_template = fs.readFileSync(dir_path+"/index.mustache.html").toString();
    var page_data={"name":"Test Bingo"};
    var result = mustache.render(page_template, page_data);  
    respons.send(result);
});

// To create competitor
app.get("/competitor", function (request, respons) {
    var page_template = fs.readFileSync(dir_path+"/competitor.mustache.html").toString();
    var result = mustache.render(page_template, {});  
    respons.send(result);
});

//  Add competitor to database
app.post("/add-competitor", function (request, respons) {
    // Random string
    var competitor_code = s4();
    connection.query("INSERT INTO competitors VALUES(NULL, \"" + competitor_code +"\", \""+request.body.name+"\", \""+request.body.vehicle_number+"\", NOW());", function(err) {
        if (err) {
            console.log("Error: ".red + err);
        }
        else {
            console.log("Add information about competitor".green+request.body.name);
            respons.redirect("/"); 
        }
    });
});

//  To list all competitor
app.get("/competitor-list", function (request, respons) {
    var page_template = fs.readFileSync(dir_path+"/competitor-list.mustache.html").toString();
    connection.query("SELECT * FROM competitors", function(err, rows) {
        if (err) {
            console.log("Error: ".red + err);
        }
        else {
            console.log("Listed information about competitor".green);
            var result = mustache.render(page_template, {"competitors":rows});  
            respons.send(result);
        }
    });
});

app.listen(port, function () {
    console.log("MC Bingo Server now listning on port".green +(" "+ port).blue);
  // connection.end();
});
