# MC bingo

Ett försöka att lösa en uppgiften som vi fick ifrån MC klubben. Jag har inte läst deras önskemål allt för ingånde utan gör bara en första utkast här för att testa tekniken. Metoderna som använda här borde fungera för den färdiga produkten men både program logic, layoute och utsende behöver förändras för att det ska bli som de vill ha det.

Jag använder mig av i huvudsak tre moduler ifrån npm:

 * express - för att vi ska få en enkel webbserver. Dessutom utökar jag denna modul med "body-parser" för att förenkla http användningen ännu mer. 
 * mustache - en "template" motor för att omvandla vårt data till html.
 * mysql - För att komunicera med en databas. Du behöver mysql installerat för att använda detta.

*Jag var inte nogran med säkerheten så det kanske du behöver kolla upp liter mer. Framförallt injections attacker i mysql.*


## Installation

### Installera MySQL först

MySQL är en databas som bygger på SQL språket. Behöver du lära dig mer om sql så titta på denna sajt: [sqlzoo](http://sqlzoo.net/). Du kan kan också behöva googla på hur du skapar en tabell och lägger till data till en tabell.

I vilket fall som helst behöver du installera mysql, gör det som fungerar på ditt system. Men nedan borde fungera på debian/ubuntu.

'''
sudo apt-get install mysql-server
'''

Tänk på att välja ett lösenord som du kommer ihåg eftersom du behöver detta för att konfiguera ditt program.

Du kanske också vill installera phpmyadmin för att hantera databasen lättare. Du behöver i sådanfall även php och apache.


### Installera själva projekt

I katalogen med projekt koden skriv.

'''
npm install
'''

Vi behöver nu skapa databaserna. Det görs genomföljande kommando:

'''
mysql -u root -p  < database-install.sql 
'''

Du behöver skriva in lösenordet här. Observera att all infromation som du sparat i databasen kommer försvinna om du gör detta igen.

Du behöver sedan också öppna "config.js" och ändra uppgifterna där så det stämmer. Det är antagligen bara lösenordet du behöver ändra. 

## Användarfall med dev kommentarer.

*Osäker om detta löser deras problem, men det är en början som man kan bygga vidare på.*

Observera att alla sidor finns med länkar ifrån första sidan. 

1. En funktionär vid start skriver in vajre anmäld förare i registret, dvs. "/competitor" och tävlingskoden till de tävlande. 

2. Behöver man få fram tävlingskoden igen så går man in på "/competitor-list" där alla data över tävlande finns.

3.  En tävlingsledare går in på "/answer" och skriver in rätt svar på .

4. När de tävlande kommer till en station så går de till sidan "/result". Där skriver de in resultatet på frågorna, fem olika och rapporterar resultatet på den fysiska delen.

5. När alla frågor är besvarade och tävlande kommit i mål så registreras de på sidan "/finish".

6. Programmet räknar nu ut resaultatet och sparar det i databasen.

7. Nu kan alla gå in på sidan "/result" där alla resultat visas i ordning.

## Att göra

* Göra en tabell i databasen som kan innehålla svar ifrån deltagarna.
* Implentera alla sidor som nämns ovan.
* Programkod för att checka resultat med mera.
* Få till bra html kod.
* Få till bra css så sidan ser snygg ut.
* Se till att sidan ser bra ut på olika typer av enheter.

## Framtida idéer

* Få detta att fungera med vad kund förväntar sig.
* Föra ett system för automatiskbackup ifall server skulle krasa.
* Få något typ av säkerhet, så tävlingledning kan gå in på sidor som inte de tävlande kan.
* Olika typer av feedback till användare som skriver in fel data.
* Möjligheten att hantera flera tävlingar.
* Än bra server installation som klara av en tävling.
