DROP DATABASE IF EXISTS mc_bingo;
CREATE DATABASE mc_bingo;
USE mc_bingo;
CREATE TABLE competitors(
                   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                   contest_code VARCHAR(10),
                   name VARCHAR(100),
                   vehicle_number VARCHAR(6),
                   time DATETIME NULL DEFAULT NOW()
);
